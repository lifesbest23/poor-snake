package lifesbest23.snake002;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GamePanel extends JPanel implements KeyListener, FocusListener, WindowListener{
	private static final long serialVersionUID = -921885097579452893L;
	
	private Snake snake;

	private boolean startUpScreen = true;
	
	private boolean running = false;
	private boolean paused = false;
	private boolean gameOver = false;
	private boolean settings = false;
	private boolean highScore = false;
	private boolean gameSettings = false;
	private boolean soundSettings = false;
	private boolean playerSettings = false;
	private boolean playerSettingsKeySelection = false;
	
	private Dimension field = new Dimension(500, 500);
	private BufferedImage backGroundImage = new BufferedImage(500, 500, BufferedImage.TYPE_3BYTE_BGR);
	private BufferedImage startUpScreenImage;
	private SnakeSnake SSnake = new SnakeSnake(0, 200, Snake.EAST, Color.GREEN, field, this);
	
	private Point food;
	private Point extraFood = null;
	
	private Color snakeColor = Color.GRAY;
	private Color snakeHeadColor = Color.BLACK;
	private Color foodColor = Color.BLACK;
	private Color extraFoodColor = new Color(200, 200, 200);
	private Color backgroundColor = Color.WHITE;
	private Color settingsColor = new Color(55, 55, 55, 150);
	private Color gameSettingsColor = new Color(100, 100, 100, 150);
	private Color soundSettingsColor = new Color(150, 150, 150, 150);
	private Color playerSettingsColor = new Color(200, 200, 200, 150);
	private Color buttonColor = Color.BLACK;
	private Color textColor = Color.WHITE;
	private Color frameColor = Color.WHITE;
	private Color selectedColor = Color.BLACK;
	private Color scoreColor = Color.BLACK;
	private Color barColor = Color.BLACK;
	
	private String colorTheme = "NORMAL";

	private int upKey = KeyEvent.VK_W;
	private int downKey = KeyEvent.VK_S;
	private int leftKey = KeyEvent.VK_A;
	private int rightKey = KeyEvent.VK_D;
	
	private boolean musicOn = true;
	private boolean soundOn = true;
	private Clip musicSource;
	private float soundVolume = 0f;
	private float musicVolume = -20f;
	
	//speedSelction ; speedIncreaseSelection ; extraFoodSelection ; extraFoodSpeedSelection
	private int[] gameSettingsSelection = {0, 0, 0, 0};
	private int[] playerSettingsSelection = {0, Integer.MAX_VALUE, leftKey, rightKey, upKey, downKey};
	private int[] soundSettingsSelection = {0, 0};
	private File soundFileSelection = null;
	
	private String[][] gameSettingsStrings = {
			{"VERY EASY", "EASY", "MEDIUM", "HARD", "HARDER", "HARDEST"}, //Speed
			{"OFF", "ON", "ALL 10", "ALL 50", "ALL 80", "ALL 100"},		//speedTypes
			{"OFF", "ON", "ALL 5", "ALL 10", "ALL 15", "INCREASING"},				//ExtraFood
			{"ENDLESS", "VERY SLOW", "SLOW", "MEDIUM", "FAST", "VERY FAST"}			//ExtraFoodSpeed
	};
	private String[][] playerSettingsStrings = {
			{"NORMAL", "NEON", "B&W", "CUSTOM"} //ColorThemes
	};
	private String[][] soundSettingsStrings = {
			{"ON", "OFF"},		//Sound
			{"ON", "OFF"}		//Songs
	};
	
	private List<HighScore> highScoreList;
	private int highScorePage = 0;
	
	private int speed = 6;				//Speed of the Snake
	private int speedIncreaseRate = -1;	//The increase rate of the snake 
	private int extraFoodMode = 1;		//Food Mode ; 1 = normal extra food in foodRate;
										//-1 food increasing; 0 = no extraFood
	private int extraFoodRate = 5;		//Point rate in which extra Food is appearing
	private int extraFoodIncreasingRate = 5;//Point Rate for mode = -1
	private int extraFoodValue = 2;		//the points increase by eating extra Food
	private int extraFoodSpeed = 6;		//Speed in which the extra food is dissappering
	
	private int bar = 0;
	private int scoreOld = 0;
	private int score = 0;
	
	private int selection = 0;
	
	private Random rand;
	
	
	public GamePanel(){
		super();
		this.setFocusable(true);
		this.setPreferredSize(field);
		new Thread(){
			public void run(){
				loadSettings();
				loadHighscore();
			}
		}.start();
		rand = new Random();
		
		//musicThread
		new Thread(){
			public void run(){
				AudioInputStream musicStream = null;
				try {
					musicStream = AudioSystem.getAudioInputStream(
							new File(getClass().getResource("/resources/sound/music.wav").getPath()));
				} catch (UnsupportedAudioFileException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}
				
				musicSource = null;
				
				try {
					musicSource = AudioSystem.getClip();
				} catch (LineUnavailableException e1) {e1.printStackTrace();}
				try {
					musicSource.open(musicStream);
				} catch (LineUnavailableException e) {e.printStackTrace();
				} catch (IOException e) {e.printStackTrace();}

				musicSource.loop(Clip.LOOP_CONTINUOUSLY);
				musicSource.start();
				
				while(true){
					if(!musicSource.isRunning() && musicOn)
						musicSource.start();
					else if(musicSource.isRunning() && !musicOn)
						musicSource.stop();
					try{
						Thread.sleep(70);
					}catch(Exception e){e.printStackTrace();}
				}
			}
		}.start();
		//startUpScreenThread
		new Thread(){
			public void run(){
				try {
					startUpScreenImage = ImageIO.read(
							getClass().getResource("/resources/image/startscreen.png"));
				} catch (IOException e1) {e1.printStackTrace();}
				int counter = 0;
				while(startUpScreen){
					counter ++;
					if(counter == 100)
						startUpScreen = false;
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}.start();
		//StartScreenThread
		new Thread(){
			public void run(){
				SSnake.run();
			}
		}.start();
	}
	
	private void setFood(){
		food = new Point(rand.nextInt(field.width - 15), rand.nextInt(field.height - 15));
	}
	
	private void setExtraFood(){
		extraFood = new Point(rand.nextInt(field.width - 16), rand.nextInt(field.height - 16));
	}
	
	private void playFoodSound(){
		if(soundOn){
			new Thread(){
				public void run(){
					AudioInputStream musicStream = null;
					try {
						musicStream = AudioSystem.getAudioInputStream(
								new File(getClass().getResource("/resources/sound/Pling01.wav").getPath()));
					} catch (UnsupportedAudioFileException e) {e.printStackTrace();
					} catch (IOException e) {e.printStackTrace();}
					
					Clip c = null;
					
					try {
						c = AudioSystem.getClip();
					} catch (LineUnavailableException e1) {e1.printStackTrace();}
					try {
						c.open(musicStream);
					} catch (LineUnavailableException e) {e.printStackTrace();
					} catch (IOException e) {e.printStackTrace();}
					
					c.start();
				}
			}.start();
		}
	}
	
	private void playExtraFoodSound(){
		if(soundOn){
			new Thread(){
				public void run(){
					AudioInputStream musicStream = null;
					try {
						musicStream = AudioSystem.getAudioInputStream(
								new File(getClass().getResource("/resources/sound/Pling02.wav").getPath()));
					} catch (UnsupportedAudioFileException e) {e.printStackTrace();
					} catch (IOException e) {e.printStackTrace();}
					
					Clip c = null;
					
					try {
						c = AudioSystem.getClip();
					} catch (LineUnavailableException e1) {e1.printStackTrace();}
					try {
						c.open(musicStream);
					} catch (LineUnavailableException e) {e.printStackTrace();
					} catch (IOException e) {e.printStackTrace();}
					
					c.start();
				}
			}.start();
		}
	}
	
	private void updateBackground(){
		if(bar > 0)
			bar -= 1;
		//when the extraFoodRate and it is not the same as last time and extraFoodMode is active
		else if(score % extraFoodRate == 0 && score != scoreOld && extraFoodMode > 0){
			setExtraFood();
			bar = 1000;
			scoreOld = score;
			new Thread(){
				public void run(){
					while(bar > 0){
						if(running && !paused){
							//Extra food and bar Update
							if( bar % 10 == 0)
								updateBackground();
							else if(bar > 0)
								bar --;
						}
						try {
							Thread.sleep(extraFoodSpeed);
						} catch (InterruptedException e) {e.printStackTrace();}
					}
					if(bar == 0){
						extraFood = null;
						updateBackground();
					}
				}
			}.start();
		}
		else if(score % extraFoodIncreasingRate == 0 && score != scoreOld && extraFoodMode < 0){
			setExtraFood();
			bar = 1000;
			scoreOld = score;
			extraFoodIncreasingRate += extraFoodRate++;
			new Thread(){
				public void run(){
					while(bar > 0){
						if(running && !paused){
							//Extra food and bar Update
							if( bar % 10 == 0)
								updateBackground();
							else if(bar > 0)
								bar --;
						}
						try {
							Thread.sleep(extraFoodSpeed);
						} catch (InterruptedException e) {e.printStackTrace();}
					}
					if(bar == 0){
						extraFood = null;
						updateBackground();
					}
				}
			}.start();
		}

		Graphics g = backGroundImage.getGraphics();
		//ClearImage
		g.setColor(backgroundColor);
		g.fillRect(0, 0, 500, 500);

		//Score
		g.setFont(new Font("Sans", Font.BOLD, 30));
		g.setColor(scoreColor);
		g.drawString(Integer.toString(score), 10, 40);
		//Extra Points Bar
		g.setColor(barColor);
		g.fillRoundRect(field.width - 110, 20, 100, 10, 15, 10);
		g.setColor(backgroundColor);
		g.fillRect(field.width - 110 + bar/10, 10, 100, 30);
		g.setColor(frameColor);
		g.drawRoundRect(field.width - 110, 20, 100, 10, 15, 10);
		//Food		
		g.setColor(foodColor);
		g.fillOval(food.x, food.y, 15, 15);
		
		if(extraFood != null){
			g.setColor(extraFoodColor);
			g.fillOval(extraFood.x, extraFood.y, 16, 16);
		}
		
	}
	
	@Override protected void paintComponent(Graphics g){
		if(startUpScreen){
			g.drawImage(startUpScreenImage, 0, 0, this);
			return;
		}
		if(!paused && running && !gameOver)
			drawRunning(g);
		else if(paused && running && !gameOver)
			drawPaused(g);
		else if(!running)
			drawStart(g);
		else if(gameOver)
			drawGameOver(g);
		
		if(settings)
			drawSettings(g);
		else if(highScore)
			drawHighScore(g);
	}
	
	private void drawRunning(Graphics g){
		g.drawImage(backGroundImage, 0, 0, this);
		
		g.setColor(snakeColor);
		int count = 0;
		for(Point p:snake.getPath()){
			if(p != null && count == 0){
				g.fillOval(p.x - 5, p.y - 5, 10, 10);
				count = 10;
			}
			count--;
		}
		
		Point p = snake.getLocation();
		g.setColor(snakeHeadColor);
		g.drawOval(p.x - 6, p.y - 6, 12, 12);
	}
	
	private void drawStart(Graphics g){
		g.setColor(backgroundColor);
		g.fillRect(0, 0, 500, 500);
		
		//draw Snake
		g.setColor(snakeColor);
		for(int i = 0; i < SSnake.getPath().length; i += 10)
			g.fillOval(SSnake.getPath()[i].x - 5, SSnake.getPath()[i].y - 5, 10, 10);
		
		Point p = SSnake.getLocation();
		g.setColor(snakeHeadColor);
		g.drawOval(p.x - 6, p.y - 6, 12, 12);
		
		g.setColor(buttonColor);
		g.fillRoundRect(145, 211, 180, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(145, 243, 180, 27, 50, 27);
		g.fillRoundRect(145, 275, 180, 27, 50, 27);
		g.fillRoundRect(145, 307, 180, 27, 50, 27);
		g.fillRoundRect(145, 361, 180, 27, 50, 27);

		//Set COlors
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
		
		//draw text "New Game"		0
		g.setColor(textColor);g.drawString("New Game", 150, 230);
		g.setColor(frameColor);
		g.drawRoundRect(145, 211, 180, 27, 50, 27);
		//draw Text "Settings"						1
		g.setColor(textColor);g.drawString("Settings", 150, 262);
		g.setColor(frameColor);
		g.drawRoundRect(145, 243, 180, 27, 50, 27);
		//draw Text "Highscore"					2
		g.setColor(textColor);g.drawString("Highscore", 150, 294);
		g.setColor(frameColor);
		g.drawRoundRect(145, 275, 180, 27, 50, 27); 
		//draw Text "About"						3
		g.setColor(textColor);g.drawString("About", 150, 326);
		g.setColor(frameColor);
		g.drawRoundRect(145, 307, 180, 27, 50, 27); //32 diff
		//draw Text "Quit Game"
		g.setColor(textColor);g.drawString("Quit Game", 150, 380);
		g.setColor(frameColor);
		g.drawRoundRect(145, 361, 180, 27, 50, 27); //32 diff
		
		//draw selected
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 4){
			g.drawRoundRect(143, 209 + selection * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + selection * 32, 182, 29, 50, 29);
		}
		else if(selection == 4){
			g.drawRoundRect(143, 199 + 5 * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 200 + 5 * 32, 182, 29, 50, 29);
		}
	}
	
	private void drawSettings(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRoundRect(50, 50, 400, 400, 50, 50);
		g.setColor(settingsColor);
		g.fillRoundRect(50, 50, 400, 400, 50, 50);
		g.setColor(frameColor);
		g.drawRoundRect(50, 50, 400, 400, 50, 50);
		
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.drawString("SETTINGS", 160, 100);
		//draw Round Rect as background for writing
		g.setColor(buttonColor);
		g.fillRoundRect(145, 211, 180, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(145, 243, 180, 27, 50, 27);
		g.fillRoundRect(145, 275, 180, 27, 50, 27);
		g.fillRoundRect(145, 307, 180, 27, 50, 27);
		
		//Set COlors
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
		
		//draw text "Game"		0
		g.setColor(textColor);g.drawString("Game Settings", 150, 230);
		g.setColor(frameColor);
		g.drawRoundRect(145, 211, 180, 27, 50, 27);
		//draw Text "Sound"						1
		g.setColor(textColor);g.drawString("Sound", 150, 262);
		g.setColor(frameColor);
		g.drawRoundRect(145, 243, 180, 27, 50, 27);
		//draw Text "Player"					2
		g.setColor(textColor);g.drawString("Player", 150, 294);
		g.setColor(frameColor);
		g.drawRoundRect(145, 275, 180, 27, 50, 27); 
		//draw Text "Back"						3
		g.setColor(textColor);g.drawString("Back", 150, 326);
		g.setColor(frameColor);
		g.drawRoundRect(145, 307, 180, 27, 50, 27); //32 diff
		
		//draw selected
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 4){
			g.drawRoundRect(143, 209 + selection * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + selection * 32, 182, 29, 50, 29);
		}
		
		if(gameSettings)
			drawGameSettings(g);
		else if(soundSettings)
			drawSoundSettings(g);
		else if(playerSettings)
			drawPlayerSettings(g);
	}
	
	private void drawGameOver(Graphics g){
		g.drawImage(backGroundImage, 0, 0, this);
		
		g.setColor(snake.getColor());
		int count = 0;
		for(Point p:snake.getPath()){
			if(p != null && count == 0){
				g.fillOval(p.x - 5, p.y - 5, 10, 10);
				count = 10;
			}
			count--;
		}
		
		//SnakeHead
		Point p = snake.getLocation();
		g.setColor(frameColor);
		g.drawOval(p.x - 6, p.y - 6, 12, 12);
		
		//GameOver Text
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 23));
		g.drawString("GAME OVER", 180, 200);
		//draw Round Rect as background for writing
		g.setColor(buttonColor);
		g.fillRoundRect(145, 211, 180, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(145, 243, 180, 27, 50, 27);

		g.fillRoundRect(145, 307, 180, 27, 50, 27);
		//Set COlors
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
		
		//draw text "Continue Game"		0
		g.setColor(textColor);g.drawString("New Game", 150, 230);
		g.setColor(frameColor);
		g.drawRoundRect(145, 211, 180, 27, 50, 27);
		//draw Text "Back to Start"						1
		g.setColor(textColor);g.drawString("Back to Start", 150, 262);
		g.setColor(frameColor);
		g.drawRoundRect(145, 243, 180, 27, 50, 27);
		
		//draw Text "Quit Game"						3
		g.setColor(textColor);g.drawString("Quit Game", 150, 326);
		g.setColor(frameColor);
		g.drawRoundRect(145, 307, 180, 27, 50, 27); //32 diff
		
		//draw selected
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 2){
			g.drawRoundRect(143, 209 + selection * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + selection * 32, 182, 29, 50, 29);
		}
		else if(selection == 2){
			g.drawRoundRect(143, 209 + 3 * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + 3 * 32, 182, 29, 50, 29);
		}
	}
	
	private void drawGameSettings(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(gameSettingsColor);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(frameColor);g.drawRoundRect(70, 110, 360, 340, 50, 50); 	//x max = 430
		
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.drawString("Game", 200, 140);
		
		g.setColor(buttonColor);
		g.fillRoundRect(90, 211, 150, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(90, 243, 150, 27, 50, 27);
		g.fillRoundRect(90, 275, 150, 27, 50, 27);
		g.fillRoundRect(90, 307, 150, 27, 50, 27);
		//g.fillRoundRect(90, 339, 150, 27, 50, 27);
		//g.fillRoundRect(90, 371, 150, 27, 50, 27);
		
		//Ok and cancel
		g.fillRoundRect(110, 403, 110, 27, 70, 27);
		g.fillRoundRect(280, 403, 110, 27, 70, 27);
		
		//Set COlors
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
				
		//draw text "Speed"		0
		g.setColor(textColor);g.drawString("Speed", 98, 230);
		g.setColor(frameColor);g.drawRoundRect(90, 211, 150, 27, 50, 27);
		//draw Text "Increasing Speed"						1
		g.setColor(textColor);g.drawString("Increasing Speed", 98, 262);
		g.setColor(frameColor);g.drawRoundRect(90, 243, 150, 27, 50, 27);
		//draw Text "Extra Food"							2
		g.setColor(textColor);g.drawString("Extra Food", 98, 294);
		g.setColor(frameColor);g.drawRoundRect(90, 275, 150, 27, 50, 27); 
		//draw Text "EXtra Food Speed"									3
		g.setColor(textColor);g.drawString("Extra Food Speed", 98, 326);
		g.setColor(frameColor);g.drawRoundRect(90, 307, 150, 27, 50, 27); //32 diff
		//g.setColor(textColor);g.drawString("Not Implemented", 98, 360);
		//g.setColor(frameColor);g.drawRoundRect(90, 339, 150, 27, 50, 27); //32 diff
		//g.setColor(textColor);g.drawString("Not Implemented", 98, 392);
		//g.setColor(frameColor);g.drawRoundRect(90, 371, 150, 27, 50, 27); //32 diff
		
		//ok and cancel
		g.setColor(textColor);g.drawString("Ok", 130,  403 + 20);
		g.setColor(frameColor);g.drawRoundRect(110, 403, 110, 27, 70, 27);
		g.setColor(textColor);g.drawString("Cancel", 295, 403 + 20);
		g.setColor(frameColor);g.drawRoundRect(280, 403, 110, 27, 70, 27);

		//drawSelected
		
		g.setFont(new Font("Serif", Font.PLAIN, 17));
		for(int i = 0; i < 4; i ++){
			g.setColor(buttonColor);
			g.fillRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(frameColor);g.drawRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(textColor);g.drawString(gameSettingsStrings[i][gameSettingsSelection[i]], 269, 230 + i * 32);
		}
		//Selection
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 4){
			g.drawRoundRect(258, 209 + selection * 32, 154, 31, 2, 2);
			g.drawRoundRect(259, 210 + selection * 32, 152, 29, 2, 2);
		}
		else if(selection == 4){
			g.drawRoundRect(108, 401, 114, 31, 70, 27);
			g.drawRoundRect(109, 402, 112, 29, 70, 27);
		}
		else if(selection == 5){
			g.drawRoundRect(278, 401, 114, 31, 70, 27);
			g.drawRoundRect(279, 402, 112, 29, 70, 27);
		}
	}
	private void drawSoundSettings(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(soundSettingsColor);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(frameColor);g.drawRoundRect(70, 110, 360, 340, 50, 50); 	//x max = 430
		
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.drawString("Sound", 200, 140);
		
		g.setColor(buttonColor);
		g.fillRoundRect(90, 211, 150, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(90, 243, 150, 27, 50, 27);
		g.fillRoundRect(145, 275, 180, 27, 50, 27);//for load music
		//g.fillRoundRect(90, 307, 150, 27, 50, 27);
		//g.fillRoundRect(90, 339, 150, 27, 50, 27);
		//g.fillRoundRect(90, 371, 150, 27, 50, 27);
		
		//Ok and cancel
		g.fillRoundRect(110, 403, 110, 27, 70, 27);
		g.fillRoundRect(280, 403, 110, 27, 70, 27);
		
		//Set COlors
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
				
		//draw text "Snake Color"		0
		g.setColor(textColor);g.drawString("Sound", 98, 230);
		g.setColor(frameColor);g.drawRoundRect(90, 211, 150, 27, 50, 27);
		//draw Text								1
		g.setColor(textColor);g.drawString("Music", 98, 262);
		g.setColor(frameColor);g.drawRoundRect(90, 243, 150, 27, 50, 27);
		//draw Text								2
		g.setColor(textColor);g.drawString("Load Music", 150, 294);
		g.setColor(frameColor);g.drawRoundRect(145, 275, 180, 27, 50, 27);
		//draw Text 							3
		//g.setColor(textColor);g.drawString("Right", 98, 326);
		//g.setColor(frameColor);g.drawRoundRect(90, 307, 150, 27, 50, 27); //32 diff
		//g.setColor(textColor);g.drawString("Up", 98, 360);
		//g.setColor(frameColor);g.drawRoundRect(90, 339, 150, 27, 50, 27); //32 diff
		//g.setColor(textColor);g.drawString("Down", 98, 392);
		//g.setColor(frameColor);g.drawRoundRect(90, 371, 150, 27, 50, 27); //32 diff
		
		//ok and cancel
		g.setColor(textColor);g.drawString("Ok", 130,  403 + 20);
		g.setColor(frameColor);g.drawRoundRect(110, 403, 110, 27, 70, 27);
		g.setColor(textColor);g.drawString("Cancel", 295, 403 + 20);
		g.setColor(frameColor);g.drawRoundRect(280, 403, 110, 27, 70, 27);
		
		g.setFont(new Font("Serif", Font.PLAIN, 17));
		for(int i = 0; i < 2; i ++){
			g.setColor(buttonColor);
			g.fillRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(frameColor);g.drawRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(textColor);g.drawString(soundSettingsStrings[i][soundSettingsSelection[i]], 269, 230 + i * 32);
		}
		
		//Selection
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 2){
			g.drawRoundRect(258, 209 + selection * 32, 154, 31, 2, 2);
			g.drawRoundRect(259, 210 + selection * 32, 152, 29, 2, 2);
		}
		else if(selection == 2){	//for selecting choosing music
			g.drawRoundRect(143, 209 + 2 * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + 2 * 32, 182, 29, 50, 29);
		}
		else if(selection == 3){	//if ok is selected
			g.drawRoundRect(108, 401, 114, 31, 70, 27);
			g.drawRoundRect(109, 402, 112, 29, 70, 27);
		}
		else if(selection == 4){	//if cancel is selected
			g.drawRoundRect(278, 401, 114, 31, 70, 27);
			g.drawRoundRect(279, 402, 112, 29, 70, 27);
		}
	}
	private void drawPlayerSettings(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(playerSettingsColor);
		g.fillRoundRect(70, 110, 360, 340, 50, 50);
		g.setColor(frameColor);g.drawRoundRect(70, 110, 360, 340, 50, 50); 	//x max = 430
		
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.drawString("Player", 200, 140);
		
		g.setColor(buttonColor);
		g.fillRoundRect(90, 211, 150, 27, 50, 27); //always 32 diff in y
		//g.fillRoundRect(90, 243, 150, 27, 50, 27);
		g.fillRoundRect(90, 275, 150, 27, 50, 27);
		g.fillRoundRect(90, 307, 150, 27, 50, 27);
		g.fillRoundRect(90, 339, 150, 27, 50, 27);
		g.fillRoundRect(90, 371, 150, 27, 50, 27);
		
		//Ok and cancel
		g.fillRoundRect(110, 403, 110, 27, 70, 27);
		g.fillRoundRect(280, 403, 110, 27, 70, 27);
		
		//Set COlors
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
				
		//draw text "Snake Color"		0
		g.setColor(textColor);g.drawString("Colortheme", 98, 230);
		g.setColor(frameColor);g.drawRoundRect(90, 211, 150, 27, 50, 27);
		//draw Text								1
		//g.setColor(textColor);g.drawString("Left", 98, 262);
		//g.setColor(frameColor);g.drawRoundRect(90, 243, 150, 27, 50, 27);
		//draw Text								2
		g.setColor(textColor);g.drawString("Left", 98, 294);
		g.setColor(frameColor);g.drawRoundRect(90, 275, 150, 27, 50, 27); 
		//draw Text 							3
		g.setColor(textColor);g.drawString("Right", 98, 326);
		g.setColor(frameColor);g.drawRoundRect(90, 307, 150, 27, 50, 27); //32 diff
		g.setColor(textColor);g.drawString("Up", 98, 360);
		g.setColor(frameColor);g.drawRoundRect(90, 339, 150, 27, 50, 27); //32 diff
		g.setColor(textColor);g.drawString("Down", 98, 392);
		g.setColor(frameColor);g.drawRoundRect(90, 371, 150, 27, 50, 27); //32 diff
		
		//ok and cancel
		g.setColor(textColor);g.drawString("Ok", 130,  403 + 20);
		g.setColor(frameColor);g.drawRoundRect(110, 403, 110, 27, 70, 27);
		g.setColor(textColor);g.drawString("Cancel", 295, 403 + 20);
		g.setColor(frameColor);g.drawRoundRect(280, 403, 110, 27, 70, 27);
		
		g.setFont(new Font("Serif", Font.PLAIN, 17));
		for(int i = 0; i < 1; i ++){
			g.setColor(buttonColor);
			g.fillRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(frameColor);g.drawRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(textColor);g.drawString(playerSettingsStrings[i][playerSettingsSelection[i]], 269, 230 + i * 32);
		}
		//for The keys
		for(int i = 2; i < 6; i++){
			g.setColor(buttonColor);
			g.fillRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(frameColor);g.drawRoundRect(260, 211 + i * 32, 150, 27, 2, 2);
			g.setColor(textColor);g.drawString(KeyEvent.getKeyText(playerSettingsSelection[i]), 269, 230 + i * 32);
		}
		
		//Selection
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 6){
			g.drawRoundRect(258, 209 + selection * 32, 154, 31, 2, 2);
			g.drawRoundRect(259, 210 + selection * 32, 152, 29, 2, 2);
		}
		else if(selection == 6){
			g.drawRoundRect(108, 401, 114, 31, 70, 27);
			g.drawRoundRect(109, 402, 112, 29, 70, 27);
		}
		else if(selection == 7){
			g.drawRoundRect(278, 401, 114, 31, 70, 27);
			g.drawRoundRect(279, 402, 112, 29, 70, 27);
		}
	}
	
	private void drawHighScore(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRoundRect(50, 50, 400, 400, 50, 50);
		g.setColor(settingsColor);
		g.fillRoundRect(50, 50, 400, 400, 50, 50);
		g.setColor(frameColor);
		g.drawRoundRect(50, 50, 400, 400, 50, 50);
		
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.drawString("HIGHSCORES", 160, 100);
		//draw Round Rect as background for writing
		g.setColor(buttonColor);
		//score
		g.fillRoundRect(100, 144, 100, 32, 4, 4);	//title
		//name
		g.fillRoundRect(200, 144, 200, 32, 4, 4);	//title
		
		g.fillRoundRect(150, 410, 200, 27, 4, 4);
		
		//Set COlors
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
		
		//draw score heading
		g.setColor(textColor);g.drawString("Score", 120, 168);	//y+24
		g.setColor(frameColor);
		g.drawRoundRect(100, 144, 100, 32, 4, 4);
		//draw name heading
		g.setColor(textColor);g.drawString("Name", 220, 168);
		g.setColor(frameColor);
		g.drawRoundRect(200, 144, 200, 32, 4, 4);
		//draw Space To Exit
		g.setColor(textColor);g.drawString("Press Space to Close", 175, 429);
		g.setColor(frameColor);
		g.drawRoundRect(150, 410, 200, 27, 4, 4);
		
		g.setFont(new Font("Serif", Font.BOLD, 17));
		//draw highScores
		for(int i = 0; i < 7; i++){
			if(highScorePage*7 + i < highScoreList.size()){
				//background
				g.setColor(buttonColor);
				g.fillRoundRect(200, 176 + i*32, 200, 32, 4, 4);
				g.fillRoundRect(100, 176 + i*32, 200, 32, 4, 4);
				HighScore tempScore = highScoreList.get(highScorePage*7 + i);
				//draw score 
				g.setColor(textColor);g.drawString(
						Integer.toString(tempScore.getScore()), 120, 200 + i*32);	//y+24
				g.setColor(frameColor);
				g.drawRoundRect(100, 176 + i*32, 100, 32, 4, 4);
				//draw name
				g.setColor(textColor);g.drawString(tempScore.getName(), 220, 200 + i*32);
				g.setColor(frameColor);
				g.drawRoundRect(200, 176 + i*32, 200, 32, 4, 4);
			}
			else{
				//background
				g.setColor(buttonColor);
				g.fillRoundRect(200, 176 + i*32, 200, 32, 4, 4);
				g.fillRoundRect(100, 176 + i*32, 200, 32, 4, 4);
				g.setColor(frameColor);
				g.drawRoundRect(100, 176 + i*32, 100, 32, 4, 4);
				g.drawRoundRect(200, 176 + i*32, 200, 32, 4, 4);
			}
		}
	}
	
	private void drawPaused(Graphics g){
		g.drawImage(backGroundImage, 0, 0, this);
		
		g.setColor(snake.getColor());
		int count = 0;
		for(Point p:snake.getPath()){
			if(p != null && count == 0){
				g.fillOval(p.x - 5, p.y - 5, 10, 10);
				count = 10;
			}
			count--;
		}
		
		//SnakeHead
		Point p = snake.getLocation();
		g.setColor(snakeHeadColor);
		g.drawOval(p.x - 6, p.y - 6, 12, 12);
		
		//Pause Text
		g.setColor(textColor);
		g.setFont(new Font("Serif", Font.BOLD, 23));
		g.drawString("PAUSE", 190, 200);
		//draw Round Rect as background for writing
		g.setColor(buttonColor);
		g.fillRoundRect(145, 211, 180, 27, 50, 27); //always 32 diff in y
		g.fillRoundRect(145, 243, 180, 27, 50, 27);
		g.fillRoundRect(145, 275, 180, 27, 50, 27);
		g.fillRoundRect(145, 307, 180, 27, 50, 27);
		
		//Set COlors
		g.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 17));
		
		//draw text "Continue Game"		0
		g.setColor(textColor);g.drawString("Continue Game", 150, 230);
		g.setColor(frameColor);g.drawRoundRect(145, 211, 180, 27, 50, 27);
		//draw Text "Settings"						1
		g.setColor(textColor);g.drawString("Settings", 150, 262);
		g.setColor(frameColor);g.drawRoundRect(145, 243, 180, 27, 50, 27);
		//draw Text "Back to Start"					2
		g.setColor(textColor);g.drawString("Back to Start", 150, 294);
		g.setColor(frameColor);g.drawRoundRect(145, 275, 180, 27, 50, 27); 
		//draw Text "Quit Game"						3
		g.setColor(textColor);g.drawString("Quit Game", 150, 326);
		g.setColor(frameColor);g.drawRoundRect(145, 307, 180, 27, 50, 27); //32 diff
		
		//draw selected
		g.setColor(selectedColor);
		if(selection >= 0 && selection < 4){
			g.drawRoundRect(143, 209 + selection * 32, 184, 31, 50, 31);
			g.drawRoundRect(144, 210 + selection * 32, 182, 29, 50, 29);
		}
	}
		
	private void startHandler(){
		switch(selection){
		case 0:
			startNewGame();
			break;
		case 1:
			openSettings();
			break;
		case 2:
			openHighscore();
			break;
		case 3:
			openAbout();
			break;
		case 4:
			exit();
			break;
		}
	}
	
	private void pauseHandler(){
		switch(selection){
		case 0:
			resumeGame();
			break;
		case 1:
			openSettings();
			break;
		case 2:
			openStartMenu();
			break;
		case 3:
			exit();
			break;
		}
		selection = 0;
	}
	
	private void gameOverHandler(){
		switch(selection){
		case 0:
			startNewGame();
			gameOver = false;
			break;
		case 1:
			openStartMenu();
			gameOver = false;
			break;
		case 2:
			exit();
			break;
		}
	}
	
	private void gameOver(){
		gameOver = true;
		String ret = JOptionPane.showInputDialog(this, "Name", "Enter Your Name:");
		if(ret == null || ret.length() == 0)
			return;
		addHighscore(ret, score);
		highScore = true;
	}
	private void settingsHandler(KeyEvent e){
		if(gameSettings){
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 5;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 5)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				if(selection == 5){
					gameSettings = false;
					selection = 0;
				}
				else if(selection == 4)
					gameSettingsOk();
				break;
			case KeyEvent.VK_LEFT:
				if(selection < 4 &&
						gameSettingsSelection[selection] > 0 &&
						gameSettingsSelection[selection] < gameSettingsStrings[selection].length)
					gameSettingsSelection[selection]--;
				break;
			case KeyEvent.VK_RIGHT:
				if(selection < 4 &&
						gameSettingsSelection[selection] >= 0 &&
						gameSettingsSelection[selection] < gameSettingsStrings[selection].length - 1)
					gameSettingsSelection[selection]++;
				break;
			}
		}
		else if(playerSettings){
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 7;
				else
					selection--;
				playerSettingsKeySelection = false;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 7)
					selection = 0;
				else
					selection++;
				playerSettingsKeySelection = false;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				playerSettingsKeySelection = false;
				if(selection == 7){
					playerSettings = false;
					selection = 0;
				}
				else if(selection == 6)
					playerSettingsOk();
				else if(selection > 1 && selection < 6)
					playerSettingsKeySelection = true;
				break;
			case KeyEvent.VK_LEFT:
				playerSettingsKeySelection = false;
				if(selection == 0 && playerSettingsSelection[selection] > 0 &&
						playerSettingsSelection[selection] < playerSettingsStrings[selection].length)
					playerSettingsSelection[selection]--;
				break;
			case KeyEvent.VK_RIGHT:
				playerSettingsKeySelection = false;
				if(selection == 0 && gameSettingsSelection[selection] >= 0 &&
						playerSettingsSelection[selection] < playerSettingsStrings[selection].length - 1)
					playerSettingsSelection[selection]++;
				break;
			default:
				if(playerSettingsKeySelection){
					playerSettingsKeySelection = false;
					playerSettingsSelection[selection] = e.getKeyCode();
				}
			}
		}
		else if(soundSettings){
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 4;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 4)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				if(selection == 4){
					soundSettings = false;
					selection = 0;
				}
				else if(selection == 3)
					soundSettingsOk();
				else if(selection == 2)
					selectMusic();
				break;
			case KeyEvent.VK_LEFT:
				if(selection < 2 &&
						soundSettingsSelection[selection] > 0 &&
						soundSettingsSelection[selection] < soundSettingsStrings[selection].length)
					soundSettingsSelection[selection]--;
				break;
			case KeyEvent.VK_RIGHT:
				if(selection < 2 &&
						soundSettingsSelection[selection] >= 0 &&
						soundSettingsSelection[selection] < soundSettingsStrings[selection].length - 1)
					soundSettingsSelection[selection]++;
				break;
			}
		}
		else{
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 3;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 3)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				settingsSpaceHandler();
				break;
			}
		}
	}
	private void settingsSpaceHandler(){
		switch(selection){
		case 0:
			gameSettings = true;
			gameSettingsLoad();
			break;
		case 1:
			soundSettings = true;
			break;
		case 2:
			playerSettings = true;
			break;
		case 3:
			settings = false;
			selection = 0;
			break;
		}
		repaint();
	}
	private void gameSettingsOk(){
		switch(gameSettingsSelection[0]){		//Speeds
		case 0:	//"VERY EASY
			speed = 15;
			break;
		case 1:	//EASY
			speed = 12;
			break;
		case 2:	//Medium
			speed = 11;
			break;
		case 3:	//Hard
			speed = 9;
			break;
		case 4:	//Harder
			speed = 6;
			break;
		case 5://HArdest
			speed = 3;
			break;
		}
		switch(gameSettingsSelection[1]){		//speedTypes
		case 0:	//Off
			speedIncreaseRate = -1;
			break;
		case 1:	//On
			speedIncreaseRate = 20;
			break;
		case 2:	//All 10
			speedIncreaseRate = 10;
			break;
		case 3:	//All 50
			speedIncreaseRate = 50;
			break;
		case 4:	//All 80
			speedIncreaseRate = 80;
			break;
		case 5:	//All 100
			speedIncreaseRate = 100;
			break;
		}
		switch(gameSettingsSelection[2]){		//ExtraFood
		case 0:	//Off
			extraFoodMode = 0;
			extraFoodRate = 0;
			extraFoodIncreasingRate = -1;
			break;
		case 1:	//On
			extraFoodMode = 1;
			extraFoodRate = 7;
			extraFoodIncreasingRate = -1;
			break;
		case 2:	//All 5
			extraFoodMode = 1;
			extraFoodRate = 5;
			extraFoodIncreasingRate = 5;
			break;
		case 3:	//All10
			extraFoodMode = 1;
			extraFoodRate = 10;
			extraFoodIncreasingRate = -1;
			break;
		case 4:	//All 15
			extraFoodMode = 1;
			extraFoodRate = 15;
			extraFoodIncreasingRate = -1;
			break;
		case 5://Increasing
			extraFoodMode = -1;
			extraFoodRate = 5;
			extraFoodIncreasingRate = score + extraFoodRate;
			break;
		}
		switch(gameSettingsSelection[3]){		//ExtraFoodSpeed
		case 0:	//ENDLESS
			extraFoodSpeed = 1000;
			break;
		case 1:	//Very SLow
			extraFoodSpeed = 12;
			break;
		case 2:	//Solow
			extraFoodSpeed = 10;
			break;
		case 3:	//MEdium
			extraFoodSpeed = 8;
			break;
		case 4:	//Fast
			extraFoodSpeed = 6;
			break;
		case 5:	//VeryFast
			extraFoodSpeed = 4;
			break;
		}
		gameSettings = false;
		selection = 0;
	}
	private void gameSettingsLoad(){
		switch(speed){		//Speeds
		case 15:	//"VERY EASY
			gameSettingsSelection[0] = 0;
			break;
		case 12:	//EASY
			gameSettingsSelection[0] = 1;
			break;
		case 11:	//Medium
			gameSettingsSelection[0] = 2;
			break;
		case 9:	//Hard
			gameSettingsSelection[0] = 3;
			break;
		case 6:	//Harder
			gameSettingsSelection[0] = 4;
			break;
		case 3://HArdest
			gameSettingsSelection[0] = 5;
			break;
		}
		switch(speedIncreaseRate){		//speedTypes
		case -1:	//Off
			gameSettingsSelection[1] = 0;
			break;
		case 20:	//On
			gameSettingsSelection[1] = 1;
			break;
		case 10:	//All 10
			gameSettingsSelection[1] = 2;
			break;
		case 50:	//All 50
			gameSettingsSelection[1] = 3;
			break;
		case 80:	//All 80
			gameSettingsSelection[1] = 4;
			break;
		case 100:	//All 100
			gameSettingsSelection[1] = 5;
			break;
		}
		//here is if, because more than one variable has to be included
		if(extraFoodMode == -1)
			gameSettingsSelection[2] = 5;
		else if(extraFoodMode == 0)
			gameSettingsSelection[2] = 0;
		else if(extraFoodMode == 1){
			if(extraFoodRate == 7)
				gameSettingsSelection[2] = 1;
			else if(extraFoodRate == 5)
				gameSettingsSelection[2] = 2;
			else if(extraFoodRate == 10)
				gameSettingsSelection[2] = 3;
			else if(extraFoodRate == 15)
				gameSettingsSelection[2] = 4;
			
		}
		switch(extraFoodSpeed){		//ExtraFoodSpeed
		case 1000:	//ENDLESS
			gameSettingsSelection[3] = 0;
			break;
		case 12:	//Very SLow
			gameSettingsSelection[3] = 1;
			break;
		case 10:	//Solow
			gameSettingsSelection[3] = 2;
			break;
		case 8:	//MEdium
			gameSettingsSelection[3] = 3;
			break;
		case 6:	//Fast
			gameSettingsSelection[3] = 4;
			break;
		case 4:	//VeryFast
			gameSettingsSelection[3] = 5;
			break;
		}
	}
	private void playerSettingsOk(){
		setColorTheme(playerSettingsStrings[0][playerSettingsSelection[0]]);
		leftKey = playerSettingsSelection[2];
		rightKey = playerSettingsSelection[3];
		upKey = playerSettingsSelection[4];
		downKey =  playerSettingsSelection[5];
		playerSettings = false;
		selection = 0;
	}
	private void soundSettingsOk(){
		//if(tempMusicSource != null)
		//	musicSource = tempMusicSource;
		if(soundSettingsSelection[0] == 0)
			soundOn = true;
		else
			soundOn = false;
		if(soundSettingsSelection[1] == 0)
			musicOn = true;
		else
			musicOn = false;
		if(soundFileSelection != null){
			AudioInputStream musicStream = null;
			try {
				musicStream = AudioSystem.getAudioInputStream(soundFileSelection);
			} catch (UnsupportedAudioFileException e) {e.printStackTrace();
			} catch (IOException e) {e.printStackTrace();}
			
			try {
				musicSource.stop();
				musicSource.close();
				musicSource.open(musicStream);
				musicSource.start();
			} catch (LineUnavailableException e) {e.printStackTrace();
			} catch (IOException e) {e.printStackTrace();}

			musicSource.loop(Clip.LOOP_CONTINUOUSLY);
			soundFileSelection = null;
		}
		soundSettings = false;
		selection = 0;
	}
	private void selectMusic(){
		JFileChooser fc = new JFileChooser();
		
		FileNameExtensionFilter wavFilter = new FileNameExtensionFilter( "WAV", "wav" );
		fc.setFileFilter( wavFilter );
		
		switch ( fc.showOpenDialog( this ) )
		{
		  case JFileChooser.APPROVE_OPTION:
		    File file = fc.getSelectedFile();
		    if(wavFilter.accept(file) && file.length() < 25_000_000L){ //File must be smaller then 25 mb
		    	soundFileSelection = file;
		    }
		    break;
		  default:
			  return;
		}
	}
	private void startNewGame(){
		this.snake = new Snake(300, 300, Snake.NORTH, snakeColor, field);
		for(int i = 0; i < 40; i ++){
			snake.addPoint();
		}
		this.score = 0;
		this.scoreOld = 0;
		
		setFood();
		updateBackground();
		
		running = true;
		paused = false;
		
		new Thread(){
			@Override public void run() {
				while(!gameOver && running){
					if(!paused && running){
						gameOver = !snake.walk();
						//if the snake is getting on the food
						if(backGroundImage.getRGB(snake.getLocation().x, snake.getLocation().y)
								== foodColor.getRGB()){
							for(int i = 0; i < 10; i ++){
								snake.addPoint();
							}
							score++;
							setFood();
							updateBackground();
							if(score % speedIncreaseRate == 0)
								if(speed > 4)
									speed--;
							playFoodSound();
						}
						//if it is getting on the extra food
						else if(backGroundImage.getRGB(snake.getLocation().x, snake.getLocation().y)
								== extraFoodColor.getRGB()){
							score += extraFoodValue;
							extraFood = null;
							bar = 0;
							updateBackground();
							if(score % speedIncreaseRate == 0)
								if(speed > 4)
									speed--;
							playExtraFoodSound();
						}
						
					}
					//repaint the entire Window
					repaint();
					try {
						Thread.sleep(speed);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
				repaint();
				if(gameOver)
					gameOver();
			}
		}.start();
	}
	private void openSettings(){
		settings = true;
		selection = 0;
	}
	private void resumeGame(){
		paused = false;
	}
	private void exit(){
		musicSource.close();
		saveSettings();
		saveHighscore();
		System.exit(0);
	}
	private void openStartMenu(){
		this.running = false;
		this.paused = true;
	}
	private void openAbout(){
		startUpScreen = true;
		repaint();
	}
	private void openHighscore(){
		highScore = true;
		repaint();
	}
	private void setColorTheme(String theme){
		try {
			Scanner sc = new Scanner(new File(
					getClass().getResource("/resources/ColorThemes/" + theme).getPath()));
			snakeColor = new Color(sc.nextInt());
			snakeHeadColor = new Color(sc.nextInt());
			foodColor = new Color(sc.nextInt());
			extraFoodColor = new Color(sc.nextInt());
			backgroundColor = new Color(sc.nextInt());
			settingsColor = new Color(sc.nextInt());
			gameSettingsColor = new Color(sc.nextInt());
			soundSettingsColor = new Color(sc.nextInt());
			playerSettingsColor = new Color(sc.nextInt());
			buttonColor = new Color(sc.nextInt());
			textColor = new Color(sc.nextInt());
			frameColor = new Color(sc.nextInt());
			selectedColor = new Color(sc.nextInt());
			scoreColor = new Color(sc.nextInt());
			barColor = new Color(sc.nextInt());
			
			sc.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
	}
	private void addHighscore(String name, int score){
		highScoreList.add(new HighScore(score, name));
		Comparator<HighScore> comparator = Collections.reverseOrder();
		Collections.sort(highScoreList, comparator);
	}
	
	private void saveSettings(){
		try {
			FileWriter writer = new FileWriter(new File(getClass().getResource("/save/settings").getPath()) );
			
			StringBuffer temp = new StringBuffer();
			temp.append("saved settings of the snake game created by lifesbest23\n");
			temp.append(colorTheme + "\n");
			
			temp.append(upKey + "\n");
			temp.append(downKey + "\n");
			temp.append(leftKey + "\n");
			temp.append(rightKey + "\n");
			
			temp.append(musicOn + "\n");
			temp.append(soundOn + "\n");
			temp.append(musicSource + "\n");
			temp.append(soundVolume + "\n");
			temp.append(musicVolume + "\n");
			
			temp.append(speed + "\n");
			temp.append(speedIncreaseRate + "\n");
			temp.append(extraFoodMode + "\n");
			temp.append(extraFoodRate + "\n");
			temp.append(extraFoodIncreasingRate + "\n");
			temp.append(extraFoodValue + "\n");
			temp.append(extraFoodSpeed + "\n");
			
			writer.write(temp.toString());
			writer.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	private void saveHighscore(){
		try {
			FileWriter writer = new FileWriter(new File(getClass().getResource("/save/highscore").getPath()) );
			
			StringBuffer temp = new StringBuffer();
			for(int i = 0; i < highScoreList.size(); i++)
				temp.append(highScoreList.get(i).toSaveString());
			
			writer.write(temp.toString());
			writer.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	private void loadSettings(){
		try {
			Scanner sc = new Scanner(
					new File(getClass().getResource("/save/settings").getPath()) );
			
			sc.useLocale(Locale.US);
			sc.nextLine();
			colorTheme = sc.nextLine();
			System.out.println(colorTheme);
			
			setColorTheme(colorTheme);
			
			upKey = sc.nextInt();
			downKey = sc.nextInt();
			leftKey = sc.nextInt();
			rightKey = sc.nextInt();
			
			musicOn = sc.nextBoolean();
			soundOn = sc.nextBoolean();
			sc.nextLine();
			sc.nextLine();	//implement a save of the last opened song
			soundVolume = Float.parseFloat(sc.nextLine());
			musicVolume = Float.parseFloat(sc.nextLine());
			
			speed = sc.nextInt();
			speedIncreaseRate = sc.nextInt();
			extraFoodMode = sc.nextInt();
			extraFoodRate = sc.nextInt();
			extraFoodIncreasingRate = sc.nextInt();
			extraFoodValue = sc.nextInt();
			extraFoodSpeed = sc.nextInt();
			
			sc.close();
		} catch (IOException e) {e.printStackTrace();
		} catch (Exception e) {e.printStackTrace();}
	}
	private void loadHighscore(){
		try{
			highScoreList = new ArrayList<HighScore>();
			Scanner sc = new Scanner(new File(
					getClass().getResource("/save/highscore").getPath()));
			while(sc.hasNextLine())
				highScoreList.add(new HighScore(sc.nextLine()));
			sc.close();
			Comparator<HighScore> comparator = Collections.reverseOrder();
			Collections.sort(highScoreList, comparator);
		}catch (IOException e){e.printStackTrace();}
	}
	
	@Override public void keyPressed(KeyEvent e) {
		if(startUpScreen && e.getKeyCode() == KeyEvent.VK_SPACE){
			startUpScreen = false;
			return;
		}
		if(!paused && running && !gameOver && !highScore){	//game is running
			int kc = e.getKeyCode();
			if(kc == downKey || kc == KeyEvent.VK_DOWN){
				if(snake.getDirection() != Snake.NORTH)
					snake.setDirection(Snake.SOUTH);
			}
			else if(kc == upKey || kc == KeyEvent.VK_UP){
				if(snake.getDirection() != Snake.SOUTH)
					snake.setDirection(Snake.NORTH);
			}
			else if(kc == leftKey || kc == KeyEvent.VK_LEFT){
				if(snake.getDirection() != Snake.EAST)
					snake.setDirection(Snake.WEST);
			}
			else if(kc == rightKey || kc == KeyEvent.VK_RIGHT){
				if(snake.getDirection() != Snake.WEST)
					snake.setDirection(Snake.EAST);
			}
			else if(kc == KeyEvent.VK_SPACE)
				paused = true;
		}
		else if(running && paused && !settings && !gameOver && !highScore){//game is paused and not gameOver
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 3;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 3)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				pauseHandler();
				break;
			}
			repaint();
		}
		else if(!running && !settings && !gameOver && !highScore){	//start screen
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 4;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 4)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				startHandler();
				break;
			}
			repaint();
		}
		else if(gameOver && !highScore){
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(selection == 0)
					selection = 2;
				else
					selection--;
				break;
			case KeyEvent.VK_DOWN:
				if(selection == 2)
					selection = 0;
				else
					selection++;
				break;
			case KeyEvent.VK_SPACE: case KeyEvent.VK_ENTER:
				gameOverHandler();
				break;
			}
			repaint();
		}
		else if(settings && !highScore){
			settingsHandler(e);
		}
		else if(highScore){
			switch(e.getKeyCode()){
			case KeyEvent.VK_RIGHT:
				if((highScorePage + 1)*7 < highScoreList.size())
					highScorePage++;
				break;
			case KeyEvent.VK_LEFT:
				if(highScorePage != 0)
					highScorePage--;
				break;
			case KeyEvent.VK_SPACE:
				highScore = false;
				repaint();
				break;
			}
		}
		try {
			Thread.sleep(10);
			
		} catch (InterruptedException e1) {e1.printStackTrace();}
	}

	@Override public void keyReleased(KeyEvent e) {}

	@Override public void keyTyped(KeyEvent e) {}

	@Override public void focusGained(FocusEvent arg0) {}

	@Override public void focusLost(FocusEvent arg0) {
		paused = true;
	}
	
	@Override public void windowActivated(WindowEvent arg0) {}

	@Override public void windowClosed(WindowEvent arg0) {}

	@Override public void windowClosing(WindowEvent arg0) {
		exit();
	}

	@Override public void windowDeactivated(WindowEvent arg0) {}

	@Override public void windowDeiconified(WindowEvent arg0) {}

	@Override public void windowIconified(WindowEvent arg0) {}

	@Override public void windowOpened(WindowEvent arg0) {}
	
	class HighScore implements Comparable<HighScore>{
		
		private int score;
		private String name;
		
		public HighScore(int score, String name){
			this(score + " " + name);
		}
		
		public HighScore(String str){
			String[] strArray = str.split(" ");
			score = Integer.parseInt(strArray[0]);
			name = "";
			for(int i = 1; i < strArray.length; i++)
				name += strArray[i];
		}
		
		public int getScore(){
			return score;
		}
		
		public String getName(){
			return name;
		}
		
		public String toSaveString(){
			return String.format("%d %s%n", score, name);
		}
		@Override public int compareTo(HighScore o) {
			return Integer.compare(this.getScore(), o.getScore());
		}
	}
}
