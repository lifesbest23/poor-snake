package lifesbest23.snake002;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Main extends JFrame{
	private static final long serialVersionUID = 6840282860346569796L;

	public Main(){
		super();
		this.setTitle("Snake002");
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		try {
			this.setIconImage(ImageIO.read((getClass().getResource("/resources/image/logo.png"))));
		} catch (IOException e) {e.printStackTrace();}
		
		this.setFocusable(true);
		
		//initMenuBar();
		initUI();
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	private void initUI(){
		GamePanel p = new GamePanel();
		
		this.addKeyListener(p);
		this.addFocusListener(p);
		this.addWindowListener(p);
		this.add(p);
	}

	public static void main(String[] args) {
		new Main();
	}

}
