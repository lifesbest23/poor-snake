package lifesbest23.snake002;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Arrays;

public class Snake {

	private SnakePoint[] path;
	private Color color;
	
	private Dimension field;

	private boolean noWalls = true;
	
	public static final int NORTH = 0;
	public static final int WEST = 1;
	public static final int SOUTH = 2;
	public static final int EAST = 3;
	
	public Snake(int x, int y, int direction, Color color, Dimension field){
		this.field = field;
		
		path = new SnakePoint[1];
		path[0] = new SnakePoint(x, y, direction);
		
		this.color = color;
		
	}
	
	public void addPoint(){
		path = Arrays.copyOf(path, path.length + 1);
		path[path.length - 1] = path[path.length - 2].getPrevious(field, 1);
	}
	
	public void setLocation(int x, int y){
		this.setLocation(new SnakePoint(x, y));
	}
	
	public void setLocation(SnakePoint location){
		for(int i = path.length - 1; i > 0; i--){
			path[i] = path[i - 1];
		}
		path[0] = location;
	}
	
	public boolean walk(){
		return walk(1);
	}
	
	public boolean walk(int walkingDistance){
		if(walkingDistance == 0)
			return false;
		
		SnakePoint p = path[0].getNext(field, walkingDistance);
		
		if(isOnPath(p))
			return false;
		
		if(!noWalls){
			if(!p.isInField(field))
				return false;
			path[0] = path[0].getNext(field, walkingDistance);
			for(int i = path.length - 1; i > 0; i--){
				path[i] = path[i-1];
			}
		}
		
		else if(noWalls){
			path[0] = path[0].getNext(field, walkingDistance);
			for(int i = path.length - 1; i > 0; i--){
				path[i] = path[i-1];
			}
		}
		
		return true;
	}
	
	public boolean walk2(){
		return walk2(1);
	}
	
	public boolean walk2(int walkingDistance){
		if(walkingDistance == 0)
			return false;
		
		SnakePoint p = path[0].getNext(field, walkingDistance);
		
		
		if(!noWalls){
			if(!p.isInField(field))
				return false;
			path[0] = path[0].getNext(field, walkingDistance);
			for(int i = path.length - 1; i > 0; i--){
				path[i] = path[i-1];
			}
		}
		
		else if(noWalls){
			path[0] = path[0].getNext(field, walkingDistance);
			for(int i = path.length - 1; i > 0; i--){
				path[i] = path[i-1];
			}
		}
		
		return true;
	}
	
	public void setDirection(int direction){
		if(direction >= 0 && direction < 4)
			path[0].setOrientation(direction);
	}
	
	public Point[] getPath(){
		return Arrays.copyOf(path, path.length);
	}
	
	public Point getLocation(){
		return path[0];
	}
	
	public int getDirection(){
		return path[0].getOrientation();
	}
	
	public Color getColor(){
		return color;
	}
	
	public boolean isOnPath(SnakePoint p){
		for(int i = 0; i < path.length; i++)
			if(path[i].isOnPoint(p))
				return true;
		return false;
	}
	
	public void setNoWalls(boolean value){
		noWalls = value;
	}
	
}
