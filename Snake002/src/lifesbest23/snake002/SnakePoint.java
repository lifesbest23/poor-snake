package lifesbest23.snake002;

import java.awt.Dimension;
import java.awt.Point;

public class SnakePoint extends Point{
	private static final long serialVersionUID = -9196444656436320575L;
	
	public static final int NORTH = 0;
	public static final int WEST = 1;
	public static final int SOUTH = 2;
	public static final int EAST = 3;
	
	public int orientation;
	
	public SnakePoint(int x, int y){
		this(x, y, NORTH);
	}
	
	public SnakePoint(SnakePoint p){
		this(p.x, p.y, p.orientation);
	}
	
	public SnakePoint(int x, int y, int orientation){
		super(x, y);
		this.setOrientation(orientation);
	}
	
	/*
	 * Moves the point about into the oriantation of the walking
	 */
	public void walk(int distance){
		switch(orientation){
		case NORTH:
			this.y = this.y - distance;
			break;
		case WEST:
			this.x = this.x - distance;
			break;
		case SOUTH:
			this.y = this.y + distance;
			break;
		case EAST:
			this.x = this.x + distance;
			break;
		}
	}
	
	public void walk(Dimension d, int distance){
		switch(orientation){
		case NORTH:
			if(this.y - distance >= 0)
				this.y = this.y - distance;
			else
				this.y = d.height - 1;
			break;
		case WEST:
			if(this.x - distance >= 0)
				this.x = this.x - distance;
			else
				this.x = d.width - 1;
			break;
		case SOUTH:
			if(this.y + distance < d.height)
				this.y = this.y + distance;
			else
				this.y = 0;
			break;
		case EAST:
			if(this.x + distance < d.width)
				this.x = this.x + distance;
			else
				this.x = 0;;
			break;
		}
	}
	
	public void walk(Dimension d){
		walk(d, 1);
	}
	
	/*
	 * orientation can only be from 0 to 3
	 */
	public void setOrientation(int orientation){
		if(orientation >= 0 && orientation < 4)
			this.orientation = orientation;
	}
	/*
	 * returns a SnakePoint in *distance distance to the current one in the opposite of orientation
	 */
	public SnakePoint getPrevious(Dimension d, int distance){
		SnakePoint p = new SnakePoint(this.x, this.y, this.orientation);
		p.walk(d, -1 * distance);
		/*
		switch(orientation){
		case SOUTH:
			if(p.y - distance >= 0)
				p.y = p.y - distance;
			else
				p.y = d.height - 1;
			break;
		case EAST:
			if(p.x - distance >= 0)
				p.x = p.x - distance;
			else
				p.x = d.width - 1;
			break;
		case NORTH:
			if(p.y + distance < d.height)
				p.y = p.y + distance;
			else
				p.y = 0;
			break;
		case WEST:
			if(p.x + distance < d.width)
				p.x = p.x + distance;
			else
				p.x = 0;;
			break;
		}*/
		
		return p;
	}
	
	public SnakePoint getNext(Dimension d, int distance){
		return getPrevious(d, -1 * distance);
	}
	
	public int getOrientation(){
		return orientation;
	}
	
	public boolean isInField(Dimension d){
		return this.x >= 0 && this.x < d.width && this.y >= 0 && this.y < d.height;
	}
	
	public boolean isOnPoint(Point p){
		return this.x == p.x && this.y == p.y;
	}
}