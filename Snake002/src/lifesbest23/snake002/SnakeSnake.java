package lifesbest23.snake002;

import java.awt.Color;
import java.awt.Dimension;

public class SnakeSnake extends Snake{
	private boolean run = true;
	private boolean pause = false;
	private GamePanel master;
	public SnakeSnake(int x, int y, int direction, Color color, Dimension field, GamePanel master) {
		super(x, y, direction, color, field);
		this.master = master;
	}
	
	public boolean walk2(){
		boolean ret = super.walk2();
		try {
			Thread.sleep(12);
		} catch (InterruptedException e) {e.printStackTrace();}
		master.repaint();
		return ret;
	}
	
	public void setPause(boolean p){
		pause = p;
	}
	
	public void run() {
		int temp = 0;
		for(int i = 0; i < 1700; i++)
			this.addPoint();
		while(run){
			if(pause)
				break;
			for(int i = 0; i < 150; i++)
				this.walk2();
			this.setDirection(NORTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(WEST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(NORTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 70; i++)
				this.walk2();
			this.setDirection(SOUTH);
			for(int i = 0; i < 100; i++)
				this.walk2();
			this.setDirection(NORTH);
			for(int i = 0; i < 100; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 50; i++){
				this.walk2();
				this.setDirection(SOUTH);
				this.walk2();
				this.walk2();
				this.setDirection(EAST);
			}
			this.setDirection(NORTH);
			for(int i = 0; i < 100; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(SOUTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(WEST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(SOUTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(NORTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 50; i++){
				this.walk2();
				this.setDirection(SOUTH);
				this.walk2();
				this.setDirection(EAST);
			}
			this.setDirection(WEST);
			for(int i = 0; i < 50; i++){
				this.walk2();
				this.setDirection(NORTH);
				this.walk2();
				this.setDirection(WEST);
			}
			for(int i = 0; i < 50; i++){
				this.walk2();
				this.setDirection(NORTH);
				this.walk2();
				this.setDirection(EAST);
			}
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(SOUTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(WEST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(SOUTH);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(WEST);
			for(int i = 0; i < 50; i++)
				this.walk2();
			this.setDirection(EAST);
			for(int i = 0; i < 70; i++)		//y = 200; x =390
				this.walk2();
			if(temp == 0){
				for(int i = 0; i < 7; i++){			//y = y + 7 * 40 = y + 280
					for(int j = 0; j < 200; j++)
						this.walk2();
					this.setDirection(SOUTH);
					for(int j = 0; j < 20; j++)
						this.walk2();
					this.setDirection(WEST);
					for(int j = 0; j < 200; j++)
						this.walk2();
					this.setDirection(SOUTH);
					for(int j = 0; j < 20; j++)
						this.walk2();
					this.setDirection(EAST);
				}
				for(int j = 0; j < 110; j++)
					this.walk2();				//y = 480; x = 0 or 499;
				this.setDirection(SOUTH);
				for(int i = 0; i < 19; i++)
					this.walk2();
				this.setDirection(EAST);
				temp = 1;
			}
			else if(temp == 1){
				for(int i = 0; i < 5; i++){			//y = y + 5 * 40 = y + 220
					for(int j = 0; j < 200; j++)
						this.walk2();
					this.setDirection(SOUTH);
					for(int j = 0; j < 20; j++)
						this.walk2();
					this.setDirection(WEST);
					for(int j = 0; j < 200; j++)
						this.walk2();
					this.setDirection(SOUTH);
					for(int j = 0; j < 20; j++)
						this.walk2();
					this.setDirection(EAST);
				}
				for(int j = 0; j < 110; j++)
					this.walk2();
				this.setDirection(SOUTH);
				this.walk2();
				this.setDirection(EAST);
				temp = 0;
			}
			
		}
		
	}
	
}
