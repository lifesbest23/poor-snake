This snake game is created by lifesbest23.
The sounds are from me too.
If you want to use the code, you are free too, but I would appreciate it if you just would inform me with a 
 small email to goofy23.2@gmail.com.
The game is a simple snake game. Controlled by the Arrow keys, the space and Enter button and for playing you can 
 choose your own keys.
You can select different difficulties in the games settings. 
In the sound menu you can even load your own music into the game, but only wav files and only smaller then 25 mb 
 because bigger ones cause performance problems.
In the player settings you can also choose color themes. 
If you want to create your own color theme, use the snakeCustomColorThemeChooser application in the download section
 and create a new "CUSTOM" file in the folder of your game.

Here is for what the lines are in case you are interested and want to edit them by your own:
	1		snakeColor
	2		snakeHeadColor
	3		foodColor
	4		extraFoodColor
	5		backgroundColor
	6		settingsColor
	7		gameSettingsColor
	8		soundSettingsColor
	9		playerSettingsColor
	10		buttonColor
	11		textColor
	12		frameColor
	13		selectedColor
	14		scoreColor
	15		barColor
	
Thats basically all. There are a few ideas I still have, but they are very time intensive to implement, 
 so i will not do that now 

25.12.2015
lifesbest23